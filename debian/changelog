get-iplayer (3.27-1) UNRELEASED; urgency=medium

  [ Jiří Paleček ]
  * Fixed watch file for new github URLs
  * New upstream version 3.27
  * Refresh shebang patch for new version

 -- Jiri Palecek <jirka@debian>  Mon, 05 Apr 2021 16:17:54 +0200

get-iplayer (3.26-2) unstable; urgency=medium

  [ Jiří Paleček ]
  * Rules-Requires-Root No
  * Comply with debian policy on shebang paths to perl
  * Recommend libvgi-pm-perl for the CGI module
  * Fix script shebangs for debian
  * Add gitignore

 -- Jiri Palecek <jpalecek@web.de>  Mon, 29 Jun 2020 15:30:58 +0200

get-iplayer (3.01-1) unstable; urgency=medium

  [ Laszlo Kajan ]
  * Non-maintainer upload.
  * New upstream release

  [ Jiøí Paleèek ]
  * New watch file for github hosting
  * New upstream version 3.01

  [ Jiri Palecek ]

  [ Jiøí Paleèek ]
  * New watch file for github hosting
  * New upstream version 3.01

  [ Jiri Palecek ]

  [ Jiøí Paleèek ]
  * Refreshed dependencies

  [ Jiri Palecek ]

 -- Jiri Palecek <jirka@debian>  Mon, 21 Aug 2017 16:02:37 +0200

get-iplayer (2.86-1) unstable; urgency=medium

  * New upstream release (Closes: #742975, #726144)
  * Standards-version 3.9.5

 -- Jonathan Wiltshire <jmw@debian.org>  Thu, 24 Apr 2014 12:37:42 +0100

get-iplayer (2.83-1) unstable; urgency=low

  * New upstream release:
    - Fix up radio URL changes by BBC
    - Use avconv if available (Closes: #684945)
  * debian/get-iplayer.docs: rename README.txt to README.md
  * Suggest libav-tools as an alternative, now that get-iplayer can use avconv
  * Normalise debian/copyright to be v1.0
  * Normalise VCS fields

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 23 Jun 2013 19:20:33 +0100

get-iplayer (2.82+git20130608-1) unstable; urgency=low

  * New upstream snapshot (Closes: #711538)

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 08 Jun 2013 22:17:12 +0100

get-iplayer (2.82+git20130519-1) unstable; urgency=low

  * New upstream snapshot (Closes: #697976)
  * Remove livetv.patch, it is integrated upstream
  * Add VCS fields to debian/control
  * Standards-version 3.9.4
  * Use debhelper compat level 9

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 19 May 2013 20:41:13 +0100

get-iplayer (2.82-2) unstable; urgency=low

  * New patch livetv.patch fixes live tv streaming after BBC changes

 -- Jonathan Wiltshire <jmw@debian.org>  Wed, 27 Jun 2012 18:56:20 +0100

get-iplayer (2.82-1) unstable; urgency=low

  * New upstream release
  * Update homepage

 -- Jonathan Wiltshire <jmw@debian.org>  Mon, 04 Jun 2012 23:40:10 +0100

get-iplayer (2.80+git20120314-1) unstable; urgency=low

  * Upstream snapshot (Closes: #653485, #611473, #652305)
  * Standards-version 3.9.3 (no changes)

 -- Jonathan Wiltshire <jmw@debian.org>  Wed, 14 Mar 2012 22:28:23 +0000

get-iplayer (2.80-2) unstable; urgency=low

  * Promote flvstreamer to dependency and add rtmpdump as primary alternative
    (Closes: #643666)
  * Preparing to add a second binary package
  * Install web PVR CGI and wrapper script

 -- Jonathan Wiltshire <jmw@debian.org>  Mon, 02 Jan 2012 22:26:36 +0000

get-iplayer (2.80-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Wiltshire <jmw@debian.org>  Tue, 30 Aug 2011 21:27:01 +0100

get-iplayer (2.79+git20110807-1) unstable; urgency=low

  * New upstream snapshot
  * Add dependency on libxml-simple-perl (Closes: #611730)
  * Patch radio4extra.patch is integrated upstream
  * Standards-Version 3.9.2 (no changes)

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 07 Aug 2011 23:00:49 +0100

get-iplayer (2.79-2) unstable; urgency=low

  * Patch for Radio 4 Extra support

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 02 Apr 2011 16:53:41 +0100

get-iplayer (2.79-1) unstable; urgency=low

  * New upstream release (closes: #608955, #608956, #608958)
  * Update my email address and remove DMUA flag
  * Standards version 3.9.1 (no changes)
  * Update debian/copyright, including formatting according to new DEP-5
    spec
  * Patches applied upstream:
      manpage
      livetv-587146

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 09 Jan 2011 20:45:39 +0000

get-iplayer (2.78-2) unstable; urgency=low

  * New patch livetv-587146 from upstream: fix live streaming of BBC One
    (Closes: #587146)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Fri, 25 Jun 2010 15:51:24 +0100

get-iplayer (2.78-1) unstable; urgency=low

  * New upstream release
      Closes: #582812, #582905
      LP: #505433
  * Update watch file
  * Patch limelight-cdn-582659 is integrated upstream
  * New patch manpage to include manual, temporarily until this is fixed
    upstream
  * Remove old NEWS

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sat, 29 May 2010 14:08:44 +0100

get-iplayer (2.76-2) unstable; urgency=low

  * Use source Format: 3.0 (quilt)
  * New patch limelight-cdn-582659 after the BBC changed their CDN paths
    (Closes: #582659) - thanks to fsck

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sun, 23 May 2010 18:14:06 +0100

get-iplayer (2.76-1) unstable; urgency=low

  * New upstream release
  * debian/source/format: explicitly declare format 1.0

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sun, 04 Apr 2010 16:06:48 +0100

get-iplayer (2.68-1) unstable; urgency=medium

  * New upstream release fixes total breakage

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Tue, 23 Feb 2010 08:13:21 +0000

get-iplayer (2.66-1) unstable; urgency=low

  * New upstream release
  * Standards version 3.8.4
  * debian/patches/no-warn-etc: integrated upstream
  * Remove quilt dependency and logic, now that there are no outstanding
    patches

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Fri, 29 Jan 2010 00:05:17 +0000

get-iplayer (2.51-1) unstable; urgency=medium

  * New upstream release fixes total breakage

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Wed, 23 Dec 2009 16:43:39 +0000

get-iplayer (2.49-1) unstable; urgency=low

  * New upstream release
  * debian/rules, debian/control: Add quilt patch system
  * Move configuration to /etc/get_iplayer (closes: #558098)
    - patch no-warn-etc: get_iplayer: load configuration from /etc and do
      not warn about it
    - debian/install: Install configuration to /etc/get_iplayer/options
    - link /var/link/get_iplayer/options to /etc/get_iplayer/options
      until upstream fix it properly
    - during installation, copy local modifications to the new options file

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Thu, 26 Nov 2009 19:08:35 +0000

get-iplayer (2.47-1) unstable; urgency=low

  * New upstream release
  * debian/control: add Recommends for AtomicParsley

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Mon, 09 Nov 2009 09:04:00 +0000

get-iplayer (2.41-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sun, 18 Oct 2009 15:03:19 +0100

get-iplayer (2.39-1) unstable; urgency=low

  * New upstream release
  * Tidy long description formatting (Closes: #543769)
  * Remove suggestion for package lame (Closes: #548342)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Fri, 25 Sep 2009 19:05:22 +0100

get-iplayer (2.36-1) unstable; urgency=low

  [ Jonathan Wiltshire ]
  * New upstream release
  * Add recommendation for libmp3-info-perl (MP3::Info)

  [ Martin Meredith ]
  * Added DM-Upload-Allowed field to control

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Thu, 17 Sep 2009 17:29:51 +0100

get-iplayer (2.26-1) unstable; urgency=low

  * New upstream version
  * Set section to video from misc
  * Add Recommends: flvstreamer, id3v2; Suggests: mplayer, ffmpeg, lame
    (Closes: #545130)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sat, 05 Sep 2009 17:53:47 +0100

get-iplayer (2.22-1) unstable; urgency=low

  * New upstream version
  * Standards-Version 3.8.3 (no changes required)
  * Install options to /var/lib/get_iplayer

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sat, 29 Aug 2009 20:52:27 +0100

get-iplayer (2.09-1) unstable; urgency=low

  * Initial release (Closes: #512754)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Mon, 13 Jul 2009 23:26:34 +0100
